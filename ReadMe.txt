
======================================================================================================================================

1. DB used is MySQL

2. data.sql - contains scripts to create required objects and sample data  

3. bms-spring-boot-data folder has the source code developed with Eclipse tool, built with Maven pom.xml.
 - Spring boot rest with JPA
 - application.properties contains MySQL Db information : update as required.
	Note: - All orders are included but can also be limited to only latest for a customer.
	      - Achieved design implementation via simple Controller - Service - JPA (avoided interfaces and multiple services). 

4. Customer.json, Supplier.json, Person.json, Company.json,Customer-Person-Company - contains rest api examples that were executed and recorded.

5.URLs
GET:
http://localhost:8080/customer/list
http://localhost:8080/supplier/list
http://localhost:8080/person/list
http://localhost:8080/company/list
GET:
http://localhost:8080/customer/1
http://localhost:8080/supplier/1
http://localhost:8080/person/1
http://localhost:8080/company/1
POST
http://localhost:8080/customer
http://localhost:8080/supplier
http://localhost:8080/person
http://localhost:8080/company
DELETE
http://localhost:8080/customer/1
http://localhost:8080/supplier/1
http://localhost:8080/person/1
http://localhost:8080/company/1

6.Git Location

https://athiraiyan@bitbucket.org/athiraiyan/bmsdemo.git

git clone https://athiraiyan@bitbucket.org/athiraiyan/bmsdemo.git

7.Create database schema in mysql in linux environment
>mysql -u root -h localhost -p
mysql>CREATE SCHEMA `test`

#Note password is empty

8.If the dialect exception is thrown please add the below lines in "application.properties"
# Allows Hibernate to generate SQL optimized for a particular DBMS
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect

9.Custom exceptions 
CompanyPersonNotBothException - A customer or supplier can be a person 
or a company, but not both.

it is thrown while adding person and company to the customer or supplier

CustomerNotFoundException-thrown when the customer is not found

======================================================================================================================================
