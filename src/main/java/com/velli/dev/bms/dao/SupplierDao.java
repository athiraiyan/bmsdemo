package com.velli.dev.bms.dao;

import org.springframework.data.repository.CrudRepository;

import com.velli.dev.bms.model.Supplier;

public interface SupplierDao extends CrudRepository<Supplier, Long> {
	
}
