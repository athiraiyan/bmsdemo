package com.velli.dev.bms.dao;

import org.springframework.data.repository.CrudRepository;

import com.velli.dev.bms.model.Person;

public interface PersonDao extends CrudRepository<Person, Long> {
	
}
