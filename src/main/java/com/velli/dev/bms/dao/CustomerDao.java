package com.velli.dev.bms.dao;

import org.springframework.data.repository.CrudRepository;

import com.velli.dev.bms.model.Customer;

public interface CustomerDao extends CrudRepository<Customer, Long> {
	
}
