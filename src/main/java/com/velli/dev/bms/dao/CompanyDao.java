package com.velli.dev.bms.dao;

import org.springframework.data.repository.CrudRepository;

import com.velli.dev.bms.model.Company;

public interface CompanyDao extends CrudRepository<Company, Long> {
	
}
