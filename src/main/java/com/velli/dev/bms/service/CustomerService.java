/**
 * 
 */
package com.velli.dev.bms.service;

import java.util.List;

import com.velli.dev.bms.exception.CompanyPersonNotBothException;
import com.velli.dev.bms.exception.CustomerNotFoundException;
import com.velli.dev.bms.model.Customer;

public interface CustomerService {

	List<Customer> getCustomer();

	Customer findById(Long id) throws CustomerNotFoundException;

	Customer save(Customer customer) throws CompanyPersonNotBothException;

	void delete(Long id);
}
