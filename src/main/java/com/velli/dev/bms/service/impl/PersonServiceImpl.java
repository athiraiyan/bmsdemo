/**
 * 
 */
package com.velli.dev.bms.service.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.velli.dev.bms.dao.PersonDao;
import com.velli.dev.bms.model.Person;
import com.velli.dev.bms.service.PersonService;
import com.velli.dev.bms.util.BMSConstants;

@Service
public class PersonServiceImpl implements PersonService {
	final Logger logger = Logger.getLogger(PersonServiceImpl.class.getName());
	@Autowired
	private PersonDao personDao;

	@Override
	public List<Person> getPerson() {
		return (List<Person>) personDao.findAll();
	}

	@Override
	public Person findById(Long id) {
		return  personDao.findOne(id);
	}

	@Override
	public Person save(Person user) {
		//logger.log(Level.INFO, BMSConstants.PERSON_INFO_SAVED );
		return  personDao.save(user);
	}

	@Override
	public void delete(Long id) {
		personDao.delete(id);
	}
}
