/**
 * 
 */
package com.velli.dev.bms.service.impl;

import java.rmi.StubNotFoundException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.velli.dev.bms.dao.CustomerDao;
import com.velli.dev.bms.exception.CompanyPersonNotBothException;
import com.velli.dev.bms.exception.CustomerNotFoundException;
import com.velli.dev.bms.model.Customer;
import com.velli.dev.bms.service.CustomerService;
import com.velli.dev.bms.util.BMSConstants;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDao customerDao;

	@Override
	public List<Customer> getCustomer() {
		return (List<Customer>) customerDao.findAll();
	}

	@Override
	public Customer findById(Long id) throws CustomerNotFoundException {
		Customer customer = customerDao.findOne(id);
		if ( customer == null)
			throw new CustomerNotFoundException("id:- " + id);
		return  customerDao.findOne(id);
	}

	@Override
	public Customer save(Customer customer) {
		if ( customer != null && customer.getCompany()!= null && customer.getPerson()!=null) {
			throw new CompanyPersonNotBothException(BMSConstants.COMPANY_PERSON_ACCEPT_ERR_MSG);
		}		
		return  customerDao.save(customer);
	}

	@Override
	public void delete(Long id) {
		customerDao.delete(id);
	}
}
