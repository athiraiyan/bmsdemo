/**
 * 
 */
package com.velli.dev.bms.service.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.velli.dev.bms.dao.CompanyDao;
import com.velli.dev.bms.exception.CompanyPersonNotBothException;
import com.velli.dev.bms.model.Company;
import com.velli.dev.bms.service.CompanyService;
import com.velli.dev.bms.util.BMSConstants;

@Service
public class CompanyServiceImpl implements CompanyService {
	final Logger logger = Logger.getLogger(CompanyServiceImpl.class.getName());
	@Autowired
	private CompanyDao companyDao;

	@Override
	public List<Company> getCompany() {
		return (List<Company>) companyDao.findAll();
	}

	@Override
	public Company findById(Long id) {
		return  companyDao.findOne(id);
	}

	@Override
	public Company save(Company company) throws CompanyPersonNotBothException {
		Company comp = companyDao.save(company);
		logger.log(Level.INFO, BMSConstants.COMPANY_INFO_SAVED );
		return  companyDao.save(company);
	}

	@Override
	public void delete(Long id) {
		companyDao.delete(id);
	}
}
