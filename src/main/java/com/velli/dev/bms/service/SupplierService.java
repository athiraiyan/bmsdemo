/**
 * 
 */
package com.velli.dev.bms.service;

import java.util.List;

import com.velli.dev.bms.exception.CompanyPersonNotBothException;
import com.velli.dev.bms.model.Supplier;

public interface SupplierService {

	List<Supplier> getSupplier();

	Supplier findById(Long id);

	Supplier save(Supplier supplier) throws CompanyPersonNotBothException;

	void delete(Long id);
}
