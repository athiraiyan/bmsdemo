/**
 * 
 */
package com.velli.dev.bms.service;

import java.util.List;

import com.velli.dev.bms.model.Person;

public interface PersonService {

	List<Person> getPerson();

	Person findById(Long id);

	Person save(Person person);

	void delete(Long id);
}
