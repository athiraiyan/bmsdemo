/**
 * 
 */
package com.velli.dev.bms.service;

import java.util.List;

import com.velli.dev.bms.model.Company;

public interface CompanyService {

	List<Company> getCompany();

	Company findById(Long id);

	Company save(Company company);

	void delete(Long id);
}
