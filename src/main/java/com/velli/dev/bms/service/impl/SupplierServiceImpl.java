/**
 * 
 */
package com.velli.dev.bms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.velli.dev.bms.dao.SupplierDao;
import com.velli.dev.bms.exception.CompanyPersonNotBothException;
import com.velli.dev.bms.model.Supplier;
import com.velli.dev.bms.service.SupplierService;
import com.velli.dev.bms.util.BMSConstants;

@Service
public class SupplierServiceImpl implements SupplierService {
	
	@Autowired
	private SupplierDao supplierDao;

	@Override
	public List<Supplier> getSupplier() {
		return (List<Supplier>) supplierDao.findAll();
	}

	@Override
	public Supplier findById(Long id) {
		return  supplierDao.findOne(id);
	}

	@Override
	public Supplier save(Supplier supplier) throws CompanyPersonNotBothException {
		if ( supplier != null && supplier.getCompany()!= null && supplier.getPerson()!=null) {
			throw new CompanyPersonNotBothException(BMSConstants.COMPANY_PERSON_ACCEPT_ERR_MSG);
		}
		return  supplierDao.save(supplier);
	}

	@Override
	public void delete(Long id) {
		supplierDao.delete(id);
	}
}
