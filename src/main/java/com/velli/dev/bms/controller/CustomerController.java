package com.velli.dev.bms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.velli.dev.bms.model.Customer;
import com.velli.dev.bms.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<Customer>> customerDetails() {
        
		List<Customer> customerDetails = customerService.getCustomer();
		return new ResponseEntity<List<Customer>>(customerDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Customer> findById(@PathVariable(name = "id", value = "id") Long id) {

		Customer customerDetail = customerService.findById(id);
		return new ResponseEntity<Customer>(customerDetail, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Customer> findById(@RequestBody Customer customerDetails) {

		Customer customerDetail = customerService.save(customerDetails);
		return new ResponseEntity<Customer>(customerDetail, HttpStatus.OK);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable(name = "id", value = "id") Long id) {

        customerService.delete(id);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
