package com.velli.dev.bms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.velli.dev.bms.model.Person;
import com.velli.dev.bms.service.PersonService;

@Controller
@RequestMapping("/person")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<Person>> personDetails() {
        
		List<Person> personDetails = personService.getPerson();
		return new ResponseEntity<List<Person>>(personDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Person> findById(@PathVariable(name = "id", value = "id") Long id) {

		Person personDetail = personService.findById(id);
		return new ResponseEntity<Person>(personDetail, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Person> findById(@RequestBody Person personDetails) {

		Person personDetail = personService.save(personDetails);
		return new ResponseEntity<Person>(personDetail, HttpStatus.OK);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable(name = "id", value = "id") Long id) {

        personService.delete(id);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
