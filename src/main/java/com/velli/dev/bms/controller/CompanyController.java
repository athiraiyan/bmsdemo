package com.velli.dev.bms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.velli.dev.bms.model.Company;
import com.velli.dev.bms.service.CompanyService;

@Controller
@RequestMapping("/company")
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<Company>> companyDetails() {
        
		List<Company> companyDetails = companyService.getCompany();
		return new ResponseEntity<List<Company>>(companyDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Company> findById(@PathVariable(name = "id", value = "id") Long id) {
		
		Company companyDetail = companyService.findById(id);
		
		return new ResponseEntity<Company>(companyDetail, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Company> findById(@RequestBody Company companyDetails) {

		Company companyDetail = companyService.save(companyDetails);
		return new ResponseEntity<Company>(companyDetail, HttpStatus.OK);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable(name = "id", value = "id") Long id) {

        companyService.delete(id);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
