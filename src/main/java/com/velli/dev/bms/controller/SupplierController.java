package com.velli.dev.bms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.velli.dev.bms.model.Supplier;
import com.velli.dev.bms.service.SupplierService;

@Controller
@RequestMapping("/supplier")
public class SupplierController {
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<Supplier>> supplierDetails() {
        
		List<Supplier> supplierDetails = supplierService.getSupplier();
		return new ResponseEntity<List<Supplier>>(supplierDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Supplier> findById(@PathVariable(name = "id", value = "id") Long id) {

		Supplier supplierDetail = supplierService.findById(id);
		return new ResponseEntity<Supplier>(supplierDetail, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Supplier> findById(@RequestBody Supplier supplierDetails) {

		Supplier supplierDetail = supplierService.save(supplierDetails);
		return new ResponseEntity<Supplier>(supplierDetail, HttpStatus.OK);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable(name = "id", value = "id") Long id) {

        supplierService.delete(id);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
