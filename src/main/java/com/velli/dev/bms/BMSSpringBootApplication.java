package com.velli.dev.bms;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to run the SpringBoot Application 
 */
@SpringBootApplication
public class BMSSpringBootApplication {

    public static void main(String[] args) {
    	final Logger logger = Logger.getLogger(BMSSpringBootApplication.class.getName());
    	logger.log(Level.INFO, "BMS SpringBoot Application Starting");
        SpringApplication.run(BMSSpringBootApplication.class, args);
    }

}