package com.velli.dev.bms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to represent data model of the Company.Associate with database table name 'company' 
 */
@Entity
@Table(name = "company")
public class Company {
	/**
	 * Primary Key of the company table
	 */
	@Id
	@Column(name="company_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="comp_name")
    private String compName;
	@Column(name="reg_num")
    private String regNum;
	
    @OneToMany(targetEntity=PhoneNumber.class, cascade=CascadeType.PERSIST)  
    private List<PhoneNumber> phoneNumbers;	
    /**
     * Create an Person with all empty fields.
     *
     */
	public Company() {
		compName = "";
		regNum = "";
	}
	/**
	 * Create a Person with compName and regNum
	 * @param compName
	 * @param regNum
	 */
	public Company(String compName, String regNum) {
		this.compName = compName;
		this.regNum = regNum;
	}
	
	public String getCompName() {
		return compName;
	}
	
	public void setCompName(String compName) {
		this.compName = compName;
	}
	
	public String getRegNum() {
		return regNum;
	}
	
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	
	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
    /**
     * True if the fistName and regNum are equal 
     */
    public boolean equals (Object right) {
    	Company company = (Company)right;
        return compName.equals(company.compName)
                && regNum.equals(company.regNum);
    }

    public int hashCode () {
        return compName.hashCode() + 3 * regNum.hashCode();
    }

    public String toString() {
        return compName + ": " + regNum ;
    }	
}
