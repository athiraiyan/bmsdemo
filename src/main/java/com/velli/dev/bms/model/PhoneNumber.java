package com.velli.dev.bms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to represent data model of the Company.Associate with database table name 'company' 
 */
@Entity
@Table(name = "phone_number")
public class PhoneNumber {
	/**
	 * Primary Key of the company table
	 */
	@Id
	@Column(name="phnum_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="area_code")
    private String areaCode;
	@Column(name="phone_num")
    private String phoneNum;
	
    /**
     * Create an Person with all empty fields.
     *
     */
	public PhoneNumber() {
		areaCode = "";
		phoneNum = "";
	}
	/**
	 * Create a PhoneNumber with areaCode and phoneNum
	 * @param areaCode
	 * @param phoneNum
	 */
	public PhoneNumber(String areaCode, String phoneNum) {
		this.areaCode = areaCode;
		this.phoneNum = phoneNum;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public String getPhoneNum() {
		return phoneNum;
	}
	
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	
    /**
     * True if the fistName and phoneNum are equal 
     */
    public boolean equals (Object right) {
    	PhoneNumber company = (PhoneNumber)right;
        return areaCode.equals(company.areaCode)
                && phoneNum.equals(company.phoneNum);
    }

    public int hashCode () {
        return areaCode.hashCode() + 3 * phoneNum.hashCode();
    }

    public String toString() {
        return areaCode + ": " + phoneNum ;
    }	
}
