package com.velli.dev.bms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to represent data model of the Customer.Associate with database table name 'customer' 
 */
@Entity
@Table(name = "customer")
public class Customer {
	/*
	 * Primary Key of the customer table
	 */
	@Id
	@Column(name="customer_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="customer_num")
    private String customerNum;
	@Column(name="last_order_date")
    private String lastOrderDate;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	private Person person;  

	@OneToOne(cascade=CascadeType.PERSIST)
	private Company company;  
	
    /**
     * Create an Customer with all empty fields.
     *
     */
	public Customer() {
		customerNum = "";
		lastOrderDate = "";
	}
	/**
	 * Create a Customer with customerNum and lastOrderDate
	 * @param customerNum
	 * @param lastOrderDate
	 */
	public Customer(String customerNum, String lastOrderDate) {
		this.customerNum = customerNum;
		this.lastOrderDate = lastOrderDate;
	}
	
	public String getCustomerNum() {
		return customerNum;
	}
	
	public void setCustomerNum(String customerNum) {
		this.customerNum = customerNum;
	}
	
	public String getLastOrderDate() {
		return lastOrderDate;
	}
	
	public void setLastOrderDate(String lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}
	
    public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	/**
     * True if the fistName and lastOrderDate are equal 
     */
    public boolean equals (Object right) {
    	Customer customer = (Customer)right;
        return customerNum.equals(customer.customerNum)
                && lastOrderDate.equals(customer.lastOrderDate);
    }

    public int hashCode () {
        return customerNum.hashCode() + 3 * lastOrderDate.hashCode();
    }

    public String toString() {
        return customerNum + ": " + lastOrderDate ;
    }	
}
