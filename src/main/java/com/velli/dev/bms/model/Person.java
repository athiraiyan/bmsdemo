package com.velli.dev.bms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to represent data model of the Person.Associate with database table name 'person' 
 */
@Entity
@Table(name = "person")
public class Person {
	/*
	 * Primary Key of the person table
	 */
	@Id
	@Column(name="person_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="first_name")
    private String firstName;
	@Column(name="last_name")
    private String lastName;
	
    @OneToMany(targetEntity=PhoneNumber.class, cascade=CascadeType.PERSIST)  
    private List<PhoneNumber> phoneNumbers;  
    /**
     * Create an Person with all empty fields.
     *
     */
	public Person() {
		firstName = "";
		lastName = "";
	}
	/**
	 * Create a Person with firstName and lastName
	 * @param firstName
	 * @param lastName
	 */
	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	
	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
    /**
     * True if the fistName and lastName are equal 
     */
    public boolean equals (Object right) {
    	Person person = (Person)right;
        return firstName.equals(person.firstName)
                && lastName.equals(person.lastName);
    }

    public int hashCode () {
        return firstName.hashCode() + 3 * lastName.hashCode();
    }

    public String toString() {
        return firstName + ": " + lastName ;
    }
}
