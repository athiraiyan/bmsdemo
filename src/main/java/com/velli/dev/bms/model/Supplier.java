package com.velli.dev.bms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Author:Anil Vellingiri
 * Created Date: 11 Nov 2019
 * Used to represent data model of the Tax.Associate with database table name 'tax' 
 */
@Entity
@Table(name = "supplier")
public class Supplier {
	/*
	 * Primary Key of the tax table
	 */
	@Id
	@Column(name="supplier_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="tax_num")
    private String taxNum;
	@Column(name="order_lead_days")
    private int orderLeadDays;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	private Person person;  

	@OneToOne(cascade=CascadeType.PERSIST)
	private Company company;	
    /**
     * Create an Tax with all empty fields.
     *
     */
	public Supplier() {
		taxNum = "";
		orderLeadDays = 0;
	}
	/**
	 * Create a Tax with taxNum and orderLeadDays
	 * @param taxNum
	 * @param orderLeadDays
	 */
	public Supplier(String taxNum, int orderLeadDays) {
		this.taxNum = taxNum;
		this.orderLeadDays = orderLeadDays;
	}
	
	public String getTaxNum() {
		return taxNum;
	}
	
	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}
	
	public int getOrderLeadDays() {
		return orderLeadDays;
	}
	
	public void setOrderLeadDays(int orderLeadDays) {
		this.orderLeadDays = orderLeadDays;
	}
	
    public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	/**
     * True if the fistName and orderLeadDays are equal 
     */
    public boolean equals (Object right) {
    	Supplier supplier = (Supplier)right;
        return taxNum.equals(supplier.taxNum)
                && orderLeadDays == supplier.orderLeadDays;
    }

    public int hashCode () {
        return taxNum.hashCode() + 3 * orderLeadDays;
    }

    public String toString() {
        return taxNum + ": " + orderLeadDays ;
    }	
}
