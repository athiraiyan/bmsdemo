package com.velli.dev.bms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * @author Anil Vellingiri
 * Create 12 Nov 2019
 * Used to throw the exception when the customer is not found for the customer id.
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException(String exception) {
		super(exception);
	}
}
