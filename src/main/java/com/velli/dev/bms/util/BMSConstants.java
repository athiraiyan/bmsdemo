package com.velli.dev.bms.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class BMSConstants {
	
	static ResourceBundle messages = ResourceBundle.getBundle("messages.properties", new Locale("en", "US"));
	
	//"Company Person both not accepted for Customer/Supplier";
	public static final String COMPANY_PERSON_ACCEPT_ERR_MSG = "Company Person both not accepted for Customer/Supplier";//messages.getString("COMPANY_PERSON_ACCEPT_ERR_MSG");
	
	//"Company Information save sucessfully";
	public static final String COMPANY_INFO_SAVED = "Company Information save sucessfully"; // messages.getString("COMPANY_INFO_SAVED");
	
	//"Person Information save sucessfully";
	public static final String PERSON_INFO_SAVED = "Person Information save sucessfully"; //messages.getString("PERSON_INFO_SAVED");
	
}
